#include <ctime>
#include <iostream>
#include "../src/bin.hh"
#include "../src/num.hh"

using namespace fast;


void do_assert(int number, int expected)
{

  if (number == expected)
    std::cout << "\033[1;32mSUCCESS\033[0m\n";
  else
    std::cout << "\033[1;31mERROR\033[0m"
      << " expected: " << expected << ", value: " << number
      << "\n";
}

void do_assert_time(double number, double expected)
{

  if (number < expected + 10)
    std::cout << "\033[1;32mSUCCESS\033[0m\n";
  else
    std::cout << "\033[1;31mERROR\033[0m"
      << " expected: " << expected << ", value: " << number
      << "\n";
}


void test_add_1()
{
  int result = 6;

  Bin a = Bin('+',
      Bin('+', Num(1), Num(1)),
      Bin('+', Num(2), Num(2)));

  int b = a.eval();

  std::cout << "1 + 1 + 2 + 2 : ";
  do_assert(b, result);
}

void test_sub_1()
{
  int result = -2;
  Bin a = Bin('-',
      Bin('-', Num(1), Num(1)),
      Bin('-', Num(4), Num(2)));

  int b = a.eval();

  std::cout << "(1 - 1) - (4 - 2) : ";
  do_assert(b, result);
}

void test_mult_1()
{
  int result = 4;
  Bin a = Bin('*',
      Bin('*', Num(1), Num(1)),
      Bin('*', Num(2), Num(2)));

  int b = a.eval();

  std::cout << "(1 * 1) * (2 * 2) : ";
  do_assert(b, result);
}

void test_mod()
{
  Bin a = Bin('%',
      Num(10),
      Num(7));

  int b = a.eval();

  std::cout << "10 % 7 : ";
  do_assert(b, 10 % 7);
}
void test_div()
{
  int result = 1;
  Bin a = Bin('/',
      Num(3),
      Bin('/', Num(30), Num(10)));

  int b = a.eval();

  std::cout << "3/(30/10): ";
  do_assert(b, result);
}
void test_time()
{
  std::clock_t start;
  double duration_first;
  double duration_second;
  start = std::clock();
  Bin('+',Bin('+', Bin('+', 
          Bin('-',Num(5), Num(0)),
          Bin('+',Num(8), Num(0))),
        Bin('*',
          Bin('+',Num(63),Num(-10)),
          Bin('+',Num(53),Num(1)))), 
      Bin('+',
        Bin('+',
          Bin('+',Num(6),Num(10)),
          Bin('+',Num(24),Num(7))),
        Bin('+',
          Bin('*',Num(42),Num(9)),
          Bin('*',Num(3),Num(5))))).eval();
  duration_first = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
  std::cout << "building time : " << duration_first << " secs\n";
  Bin('+',Bin('+', Bin('+', 
          Bin('-',Num(5), Num(0)),
          Bin('+',Num(8), Num(0))),
        Bin('*',
          Bin('+',Num(63),Num(-10)),
          Bin('+',Num(53),Num(1)))), 
      Bin('+',
        Bin('+',
          Bin('+',Num(6),Num(10)),
          Bin('+',Num(24),Num(7))),
        Bin('+',
          Bin('*',Num(42),Num(9)),
          Bin('*',Num(3),Num(5))))).eval();
  start = std::clock();
  duration_second = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
  std::cout << "building time : " << duration_second << " secs\n";
  do_assert_time(duration_second, duration_first);
}
int main()
{
  std::cout << "*** TEST ADDITION ***\n";
  test_add_1();
  std::cout << "*** TEST SUBSTRACTION ***\n";
  test_sub_1();
  std::cout << "*** TEST MULTIPLICATION ***\n";
  test_mult_1();
  std::cout << "*** TEST MODULO ***\n";
  test_mod();
  std::cout << "*** TEST DIVISION ***\n";
  test_div();
  std::cout << "*** TEST if the node are reevaluated***\n";
  test_time();
  return 0;
}

