/**
 ** \file num.hxx
 ** \brief hxx noeud num
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** le noeud Nin
 ** equivalent a une feuille d'arbre binaire complet
 **/

#pragma once

#include <boost/functional/hash.hpp>
#include "num.hh"

namespace fast{

  inline
    Num::Num(int n):
      number_(n)
    {}
  inline
    int Num::number_get() const{
      return number_;
    }

  inline
    std::size_t hash_value(Num const& n){
      static boost::hash<int> hasher;
      return hasher(n.number_get());
    }

  inline
    bool operator==(const Num& rhs, const Num& lhs){
      return rhs.number_get() == lhs.number_get();
    }

  inline
    Num::operator int() const{
      return number_;
    }
}
