/**
 ** \file bin.hh
 ** \brief hearder noeud bin
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** le noeud bin qui s'ocuque de se calculer.
 ** equivalent a un noeud classique d'arbre binaire complet
 **/

#pragma once

#include <boost/flyweight/flyweight.hpp>      // class template flyweight
#include <boost/flyweight/hashed_factory.hpp> // hashed flyweight factory
#include <boost/flyweight/key_value.hpp>
#include <boost/flyweight/static_holder.hpp>  // regular factory instantiation
#include <boost/flyweight/simple_locking.hpp> // simple locking policy
#include <boost/flyweight/no_tracking.hpp>     // refcounting tracking policy
#include <boost/variant.hpp>

#include "fwd.hh"
#include "num.hh"

namespace fast{

  class Bin{
    /// boost variant
    using Exp = boost::variant<Num, boost::recursive_wrapper<Bin>>;
   /// flyweight de variant
    using flyExp = boost::flyweight<boost::flyweights::key_value<int,Exp>, boost::flyweights::no_tracking>;

    protected:
      flyExp r_exp_; /**< fils droit, memo : pour pas confond droite et gauche*/
      flyExp l_exp_; /**< fils gauche*/
      char op_;
      int result_;
      bool eval_ = false; /**< si on a deja evaluer le noeud*/

    public:
      /** @{ /brief de cette maniere je ne fait pas de duplication de 
       *    code pour le constructeur.
       * /param r est soit un int soit un Num soit un Bin
       * /param l est soit un int soit un Num soit un Bin
       */
      template<class A, class B>
        Bin(char op, A& r, B& l);
      template<class A, class B>
        Bin(char op, A&& r, B&& l);
      ///@}
      /** {@ /brief evalue l'arbre
       */
      int eval();
      int eval() const;
      /// @}

      /** obligatoire pour le flyweight
       * @{
       */
      friend std::size_t hash_value(Bin const& n);
      friend bool operator==(const Bin& lhs, const Bin& rhs);
      friend bool operator==(const Num& lhs, const Bin& rhs);
      friend bool operator==(const Bin& lhs, const Num& rhs);
      operator int();
      /*@}
       */
  };
} //namespace fast
#include "bin.hxx"
