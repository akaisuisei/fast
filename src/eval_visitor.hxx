/**
 ** \file eval_visitor.hxx
 ** \brief hxx du visiteur EvalVisitor
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** permet de visiter chaque noeud de l'arbre et d'appleller son evaluation
 **/

#pragma once 
#include "eval_visitor.hh"
#include "num.hh"
#include "bin.hh"

namespace fast{

  inline
    int EvalVisitor::operator()(Num& n) const{
      return n.number_get();
    }

  inline
    int EvalVisitor::operator()(Bin& b) const{
      return b.eval();
    }
  inline
    int EvalVisitor::operator()(const Num& n) const{
      return n.number_get();
    }
  inline
    int EvalVisitor::operator()(const Bin& b) const{
      return b.eval();
    }

} // namespace fast
