/**
 ** \file num.hh
 ** \brief header noeud num
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** le noeud Nin
 ** equivalent a une feuille d'arbre binaire complet
 **/

#pragma once

namespace fast{

  class Num{

    protected:
      int number_;

    public:
      Num(int n);
      int number_get() const;
      // need to use key_value with flyweight
      operator int() const;
  };
  // need an hash to use flyweight 
  std::size_t hash_value(Num const& n);
} // namespace fast
# include "num.hxx"
