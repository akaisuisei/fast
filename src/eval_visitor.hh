/**
 ** \file eval_visitor.hh
 ** \brief hearder du visiteur EvalVisitor
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** permet de visiter chaque noeud de l'arbre et d'appleller son evaluation
 **/

#pragma once
#include "boost/variant.hpp"
#include "fwd.hh"

namespace fast{

  class EvalVisitor : public boost::static_visitor<int>{

  public:
    int operator()(Num& n) const;
    int operator()(Bin& b) const;
    int operator()(const Num& n) const;
    int operator()(const Bin& b) const;
  };
} //namespace fast
# include "eval_visitor.hxx"
