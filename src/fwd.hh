/**
 ** \file fwd
 ** \brief forward declaration
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** declasre les classe du dossier permet la compilation plus simple
 **
 **/
#pragma once

namespace fast{
  class Bin;
  class Num;
  class EvalVisitor;
} // namespace fast
