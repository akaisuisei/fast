/**
 ** \file bin.hxx
 ** \brief hxx noeud bin
 ** \author kamwaw_b
 ** \version 0.1
 ** \date 16 juin 2015
 **
 ** le noeud bin qui s'ocuque de se calculer.
 ** equivalent a un noeud classique d'arbre binaire complet
 **/

#pragma once
#include "bin.hh"
#include "eval_visitor.hh"

namespace fast{

  template<class A, class B>
    inline
      Bin::Bin(char op, A& l, B& r):
        r_exp_(r),
        l_exp_(l),
      op_(op){}

  template<class A, class B>
    inline
      Bin::Bin(char op, A&& l, B&& r):
        r_exp_(r),
        l_exp_(l),
      op_(op){}
/**
 * /brief pas de duplication de code pour cette parti
 * /return fonction du carractere associer.
 */
  static inline
    std::function<int (int, int)> op_func(char op){
      if (op == '+')
        return ([] (int a, int b){ return a + b;});
      else if (op == '-')
        return [] (int a, int b){ return a - b;};
      else if (op == '*')
        return [] (int a, int b){ return a * b;};
      else if (op == '/')
        return [] (int a, int b){ return a / b;};
      else if (op == '%')
        return [] (int a, int b){ return a % b;};
      else
        return [] (int a, int b){ return a + b;};
    }

  inline
    int Bin::eval() const{
      static EvalVisitor eval_;
      return op_func(op_)(boost::apply_visitor(eval_, l_exp_.get()),
            boost::apply_visitor(eval_, r_exp_.get()));
    }

  inline
    int Bin::eval(){
        if (eval_)
          return result_;
      eval_ = true;
      static EvalVisitor eval_;
      result_ = op_func(op_)(boost::apply_visitor(eval_, l_exp_.get()),
            boost::apply_visitor(eval_, r_exp_.get()));
      return result_;
    }

  inline
    std::size_t hash_value(Bin const& b){
      boost::hash<char> hasher;
      return hasher(b.op_);
    }

  inline
    bool operator==(const Bin& lhs, const Bin& rhs){
      return lhs.op_ == rhs.op_
        && lhs.r_exp_ == rhs.r_exp_
        && lhs.l_exp_ == lhs.l_exp_;
    }

  inline
    bool operator==(const Num& lhs, const Bin& rhs){
      return rhs.eval_ && lhs.number_get() == rhs.result_ ;
    }

// je ne voulais pas dupliquer du code
  inline
    bool operator==(const Bin& lhs, const Num& rhs){
      return rhs == lhs;
    }

  inline
    Bin::operator int(){
      return eval();
    }
} //namespace fast
