#include <iostream>
#include <ctime>
#include "../src/bin.hh"
#include "../src/num.hh"

using namespace fast;

namespace fast_bench {

  void do_assert(int number, int expected)
  {

    if (number == expected)
      std::cout << "\033[1;32mSUCCESS\033[0m\n";
    else
      std::cout << "\033[1;31mERROR\033[0m\n";
  }

  void bench_same_node()
  {
    std::clock_t start;
    double duration;

    size_t n = 10000;
    size_t m = 100;
    Bin exp = Bin('+',
        Bin('+', Num(0), Num(0)),
        Bin('+', Num(0), Num(0)));
    std::cout << "### benchmark with duplicated node ###\n";

    start = std::clock();
    for (unsigned j = 1; j <= n; ++j)
    {
      Bin e1 = Bin('*',
          Bin('+', Num(1+(j%m)), Num(1+(j%m))),
          Bin('+', Num(2+(j%m)), Num(2+(j%m))));

      Bin e2 = Bin('*',
          Bin('+', Num(1+(j%m)), Num(1+(j%m))),
          Bin('+', Num(2+(j%m)), Num(2+(j%m))));

      Bin f = Bin('/', e1, e2);
      exp = Bin('+', exp, f);
    }
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "building time : " << duration << " secs\n";
    start = std::clock();
    int val = exp.eval();
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "result : " << val << " ";
    do_assert(val, n);
    std::cout << "evaluation time : " << duration << " secs\n";
  }

  void bench_diff_node()
  {
    std::clock_t start;
    double duration;

    size_t n = 10000;
    size_t m = 100;
    Bin exp = Bin('+',
        Bin('+', Num(0), Num(0)),
        Bin('+', Num(0), Num(0)));
    std::cout << "### benchmark without duplicated node ###\n";

    start = std::clock();
    for (unsigned j = 1; j <= n; ++j)
    {
      Bin e1 = Bin('*',
          Bin('+', Num(1+(j%m)), Num(1+(j%m))),
          Bin('+', Num(2+(j%m)), Num(2+(j%m))));

      Bin e2 = Bin('*',
          Bin('+', Num(1+(j%m)), Num(1+(j%m))),
          Bin('+', Num(4+(j%m)), Num(4+(j%m))));

      Bin f = Bin('/', e1, e2);
      exp = Bin('+', exp, f);
    }
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "building time : " << duration << " secs\n";
    start = std::clock();
    int val = exp.eval();
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "result : " << val << " ";
    std::cout << "evaluation time : " << duration << " secs\n";
  }

  void bench_node()
  {
    std::clock_t start;
    double duration;
    double average;
    size_t n = 1000000;
    std::cout << "### benchmark create the same node and \n"
      << "evaluated it "<< n << "time ###\n";
    start = std::clock();
    Bin('+',
        Bin('+', Num(0), Num(0)),
        Bin('+', Num(0), Num(0))).eval();
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "building time : " << duration << " secs\n";

    start = std::clock();
    for (unsigned j = 1; j <= n; ++j)
      Bin('+',
          Bin('+', Num(0), Num(0)),
          Bin('+', Num(0), Num(0))).eval();
    double total = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "building time : " << duration << " secs\n";
    average = total/(double)n;
    std::cout << "estemated average time : " << average << " secs\n";
    if (average < duration)
      std::cout << "\033[1;32mSUCCESS\033[0m\n";
    else
      std::cout << "\033[1;31mERROR\033[0m\n";
  }
}
int main()
  {
    fast_bench::bench_same_node();
    std::cout << "\n\n";
    fast_bench::bench_diff_node();
    std::cout << "\n\n";
    fast_bench::bench_node();
    return 0;
  }
// namespace fast_bench
