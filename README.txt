## Explication des algorithmes

Voici notre super projet Fast.!
C'est un arbre binaire de calcul arithm�tique sans
h�ritage, et bien s�r rapide.
Ce projet utilise boost 1.58

Pour inclure notre projet a un autre il faut juste include 
le fichier all.hh ou les deux fichier bin.hh et num.hh.

Tout d'abord, nous avons un arbre binaire sans h�ritage
avec seulement le noeud bin et la feuille num.
Pour �viter l'h�ritage nous avons opter pour un arbre qui
utilise boost variant.
Chaque enfant d'un bin est un variant. De ce fait, on n'utilise pas
l'h�ritage.
A la place de cela on utilise de la surcharge de fonction,
qui est plus rapide que l'heritage comme on utilise pas de vtable.

Pour r�duire encore plus le temps d'execution, on utilise la m�moization.
C'est un proced� o� l'on va enregistrer les valeurs
d�j� calcul�es afin de ne pas les recalculer.

Pour r�duire la taille des arbres, on a utilis� le design pattern flyweigh.
En utilisant boost flyweigh, on avait pas besoin de calculer tout les noeuds.
On commence une partie de l'�valuation de l'arbre pendant sa cr�ation. De cette
mani�re, on ne va pas cr�er de noeud inutile. Cela nous permet
de ne pas cr�er des noeuds num si un noeud bin a d�j� la m�me valeur.
On a configur� le flyweight de boost afin de l'utiliser pour la m�moization
et la creation de l'arbre.

Nous n'avons pas oblig� l utilisation d'ecrire Num pour construire un Num. 
On peux le creer implicitement.

Faute de temps et surtout de partiels (qui sont en train de se d�rouler),
nous n'avons pas pu explorer certaine piste.

### Les possibles am�liorations

#### METAPROGRAMATION

Nous aurions bien voulu essayer de faire de la m�taprogramation sur la partie
charact�re du noeud bin. En effet, nous pensons que cela aurait �t� possible de templater
le noeud bin avec une structure qui �tait specialis�e sur certain carat�re
et faire des gestions � la compilation des mauvais charact�res. De cette mani�re,
un utilisateur aurai pu ajouter des nouvelles fonctionalit�s de calcul faciliement.

#### THREAD

Nous aurions bien voulu utiliser des threads mais � un moment, nous avons utiis� une certaine variante
de singleton. On ne voulais pas privil�gier la vitesse au d�priment des calculs.

De plus le flyweight utilise des threads. On n'a pas eu le temps de lire cette documentation.

#### TEMPLATE 

Nous aurions aussi voulu essayer de templater les donn�es de l'arbre. De cette mani�re,
on aurait pu g�rer tout type de donn�es.

#### L'OPERATEUR

Pour le charact�re designant la fonction � utiliser, on se disait pendant un moment qu'il fallait
utiliser un tableau de fonction qui retournerait la fonction correspondante au charact�re. Mais on l'a pas fait.

#### MODELISATION

Une modelisation state au lieu de notre mod�lisation actuelle : Un arbre binaire complet
ne peux avoir que deux �tats (avec ou sans fils).


