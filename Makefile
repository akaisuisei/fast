CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -pedantic -std=c++1y -pthread
SRC=test/test.cc
BENCH=bench/bench.cc
LDFLAGS=-L/usr/lib/boost/lib
ARCHIVE=files.tar.bz2

check: $(SRC)
		$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^
		valgrind ./check 1> /dev/null
		strace -o log_test ./check 1> /dev/null
		./check


bench: $(BENCH)
		$(CXX) $(CXXFLAGS) $(LDFLAGS) -Ofast -o test_bench $^
		./test_bench

tar:
		rm -rf kamwaw_b-gosse_k; mkdir kamwaw_b-gosse_k
		cp -r bench kamwaw_b-gosse_k/.
		cp -r test kamwaw_b-gosse_k/.
		cp -r src kamwaw_b-gosse_k/.
		cp README.txt kamwaw_b-gosse_k/.
		cp Makefile kamwaw_b-gosse_k/.
		cp TODO kamwaw_b-gosse_k/.
		cp AUTHORS kamwaw_b-gosse_k/.
		tar -jcvf kamwaw_b-gosse_k.tar.bz2 kamwaw_b-gosse_k/

clean:
		$(RM) $(SRC:.cc=.o) check benchmark $(ARCHIVE)
		rm -rf kamwaw_b-gosse_k.tar.bz2 kamwaw_b-gosse_k

.PHONY: clean tar

